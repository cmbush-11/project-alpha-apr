# Generated by Django 4.2.2 on 2023-06-07 21:55

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("projects", "0002_taskform"),
    ]

    operations = [
        migrations.RenameModel(
            old_name="TaskForm",
            new_name="Task",
        ),
    ]
